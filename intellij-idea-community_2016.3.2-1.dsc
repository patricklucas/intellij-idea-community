-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2016.3.2-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 24860fcf28517431be055b7b8f20b0dea8b08f84 5637 intellij-idea-community_2016.3.2.orig.tar.gz
 e24e348daa2f4073892c592ddbfca8303e82408e 5776 intellij-idea-community_2016.3.2-1.debian.tar.xz
Checksums-Sha256:
 41958f809fdb7ce6640143c04c88233d51ab86e66be5c0a9e30e846af332550b 5637 intellij-idea-community_2016.3.2.orig.tar.gz
 02809fe73ebcaf1f0e04b47e8a4cdc1f111b29822d2d2efeabf985dd053e02a6 5776 intellij-idea-community_2016.3.2-1.debian.tar.xz
Files:
 e105950dd78a2260639bd06b848e4f3d 5637 intellij-idea-community_2016.3.2.orig.tar.gz
 11fb214c011eae2e855c83a41acace86 5776 intellij-idea-community_2016.3.2-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQI9BAEBCAAnBQJYZuNxIBxtYXJjZWxtaWNoYWVsa2FwZmVyQHlhaG9vLmNvLm56
AAoJECuF2UqwCcGYLNgP/0DTBKBhPbfozRvcxbiWR6TUqY8KjFZ45CUIvI36Gxlh
mhbUExq4UoSf7AhyAqegc5VynGALRKgA7OEpMHy5MI9w8UxkIt0UzJAx1+cXg+Ux
FJUJ3sDSqWRJMI2TSXNSRaZ2z7Gk2PzVmJ7l5e9TvAFX8uCvdSrb0O/SfUeI4sl4
cBVieRt27bY0Ok/gWDNmsUzmHRadMygZggVxPHitbpDF/MngslFPEMRQuhXiRDQ4
PFUBg1ZF2PyAkv57VKaIoQz9kX6jG0X+Z3bIkqj7JKD/dTMFj/xTXLpgSfiGN69p
cSz40WUgODfBKFH5aSVDk4TPlE8KrrtbDLZ6dgSdXB6WLOnPKfN/DIayegsbXFBQ
8Tc9WpuD6VAaAjfZNrv+XiOFyoMIkmh930NhsNI84I2E/kkG+DvUgCQp8iBBGqro
nYNtPb+/u0j/7zdiKEYlk5E8F2Cmzgz/GF/oVJwuiljgEL6JI3L0ZiX5qgpxw+RE
tK/X1W3dpS46wDGsja7xGhGFpszw6z34bq5fhxt4nhTt1V6SmSusIWxArJMoI/HB
5SCk7ZUnfYl93HaFR9qzomLey6as6FoWV60J3txx07A9qWu6yRwL/D1lgaDlYFt/
UchZSsjhgAY/DRJR7EBwghKWvv9vft+XWjOwUku3oGxqBNtAhXxOCz2pPZ7hksDb
=Hh25
-----END PGP SIGNATURE-----
